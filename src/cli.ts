#!/usr/bin/env node
/**
 *
 * BEGIN HEADER
 *
 * Contains:        markdown-binder command line interface
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This command processes all Markdown files in a directory, performs operations on the content and concatenates them.
 *
 *                  - YAML frontmatter is validated against either the built-in schema or a valid JSONSchema and optionally written as a separate file.
 *                  - Optionally, a document revision history table can be generated from Semver git tags;
 *                    - The folder containing the documents does not have to be the root of a git repository.
 *                    - You must provide an a list of approvers in YAML format.
 *
 *                  The output Markdown file will be named according to the 'title' property in YAML frontmatter, or failing that the name of the directory.
 *
 * END HEADER
 */
// eslint-disable-next-line node/shebang
import bindMarkdown, { writeOut } from './index'
import { orderBy } from 'natural-orderby'
import fs from 'fs'
import glob from 'glob'
import path from 'path'
import util from 'util'
import YAML from 'yaml'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'

import checkDir from './util/checkDir'
import type { Approvers } from '@lackadaisical/revision-history'

const readFileAsync = util.promisify(fs.readFile)

interface configuration extends Record<string, unknown> {
  customMetadata: Record<string, unknown> | undefined
  customSchema: Record<string, Record<string, unknown>> | undefined
  inputDir: string
  outputDir: string | undefined
  pandocDefaults: Record<string, unknown> | undefined
  revisionHistory: Approvers | undefined
  writeClean: boolean | undefined
  writeDefault: boolean | undefined
  writeYaml: boolean | undefined
}

/**
 * Function to handle being called as a command line binary.
 *
 * @param {object} argv The command line arguments that node was called with.
 */
export async function processCLI(argv: Arguments): Promise<void> {
  const configuration: configuration = {
    customMetadata: undefined,
    customSchema: undefined,
    inputDir: '',
    outputDir: undefined,
    pandocDefaults: undefined,
    revisionHistory: undefined,
    writeClean: undefined,
    writeDefault: undefined,
    writeYaml: undefined,
  }

  configuration.inputDir = path.resolve(argv.i)
  configuration.outputDir = argv.o ? path.resolve(argv.o) : path.join(configuration.inputDir, 'build')
  configuration.writeClean = argv.c ? true : undefined
  configuration.writeDefault = argv.w || argv.d ? true : undefined
  configuration.writeYaml = argv.f ? true : undefined

  if (argv.m) {
    configuration.customMetadata = YAML.parse(await readFileAsync(path.resolve(argv.m), 'utf-8')) as Record<
      string,
      unknown
    >
  }

  if (argv.d) {
    configuration.pandocDefaults = YAML.parse(await readFileAsync(path.resolve(argv.d), 'utf-8')) as Record<
      string,
      unknown
    >
  }

  if (argv.s) {
    configuration.customSchema = JSON.parse(await readFileAsync(path.resolve(argv.s), 'utf-8')) as Record<
      string,
      Record<string, unknown>
    >
  }

  if (argv.r) {
    configuration.revisionHistory = YAML.parse(await readFileAsync(path.resolve(argv.r), 'utf-8')) as Approvers
  }

  if (argv.verbose) {
    console.log('markdown-binder configuration:')
    console.log(configuration)
  }

  if (configuration.inputDir === configuration.outputDir) {
    console.error('Error: Input directory must not be output directory.')
  }

  checkDir(configuration.inputDir)
  checkDir(configuration.outputDir)

  // Get our files
  const files: string[] = glob.sync(`${configuration.inputDir}/**/*.[mM][Dd]`)
  const sortedFiles = orderBy(files).filter((file) => {
    return argv.o === undefined ? !file.includes('build') : file
  })

  if (argv.verbose) {
    console.log('Sorted Files:')
    console.log(sortedFiles)
  }

  const content = await bindMarkdown(sortedFiles, {
    customMetadata: configuration.customMetadata,
    customSchema: configuration.customSchema,
    defaultsBase: configuration.pandocDefaults,
    revisionHistory: configuration.revisionHistory,
  })

  if (argv.verbose) {
    console.log('Content to Write out:')
    console.log(content)
  }

  await writeOut(
    content,
    configuration.outputDir,
    configuration.writeClean,
    configuration.writeDefault,
    configuration.writeYaml
  )
}

interface Arguments {
  [x: string]: unknown
  c: boolean | undefined
  d: string | undefined
  f: boolean | undefined
  i: string
  m: string | undefined
  o: string | undefined
  r: string | undefined
  s: string | undefined
  verbose: boolean | undefined
  w: boolean | undefined
  $0: string
}

const argv: Arguments = yargs(hideBin(process.argv))
  .options({
    c: { type: 'boolean', alias: 'write-clean', default: false, describe: 'Strip frontmatter from document content?' },
    d: { type: 'string', alias: 'pandoc-defaults-base', describe: 'Pandoc defaults base configuration' },
    f: {
      type: 'boolean',
      alias: 'separate-frontmatter',
      describe: 'Output frontmatter as a separate YAML metadata-file',
    },
    i: { type: 'string', demandOption: true, alias: 'input-directory', describe: 'Input Directory' },
    m: { type: 'string', alias: 'custom-metadata', describe: 'Use custom metadata file' },
    o: { type: 'string', alias: 'output-directory', describe: 'Output Directory' },
    r: { type: 'string', alias: 'revision-history', describe: 'Path to revision history approvers YAML.' },
    s: { type: 'string', alias: 'custom-schema', describe: 'JSONSchema to validate Markdown frontmatter against' },
    verbose: { type: 'boolean', default: false, describe: 'Request verbose output' },
    w: { type: 'boolean', alias: 'write-defaults', describe: 'Write pandoc defaults file' },
  })
  .usage(
    '$0 -i «path/to/input-dir» -o «path/to/output-dir» -f -r «path/to/custom/approvers.yaml» -d -m «path/to/custom/metadata.yaml» -s «path/to/schema.json» '
  )
  .help('h')
  .alias('h', 'help')
  .parseSync()

void processCLI(argv)
