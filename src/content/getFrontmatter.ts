/**
 * BEGIN HEADER
 *
 * Contains:        Utility class
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This file contains frontmatter parsing and validation functionality.
 *
 * END HEADER
 */

import { DateTime } from 'luxon'
import Ajv, { DefinedError, JSONSchemaType } from 'ajv'
import matter, { GrayMatterFile } from 'gray-matter'
import getDateSuffix from '../util/getDateSuffix'

type BuiltInSchema = {
  keywords: string[]
  subject: string
  title: string
  tocdepth?: number
}

class ValidationError extends Error {
  constructor(message: string) {
    super(message)
    this.name = 'ValidationError'
  }
}

const builtInSchema: JSONSchemaType<BuiltInSchema> = {
  properties: {
    keywords: {
      items: {
        type: 'string',
      },
      type: 'array',
    },
    subject: {
      type: 'string',
    },
    title: {
      type: 'string',
    },
    tocdepth: {
      maximum: 5,
      minimum: 2,
      nullable: true,
      type: 'number',
    },
  },
  required: ['title', 'subject', 'keywords'],
  type: 'object',
}

/**
 * Takes a JSON Date object and returns a string in the format
 * ##th of Month, YYY*
 *
 * @param   {string}  dateString      The date to be converted.
 * @returns  {string}            A stringified date.
 */
function fixDate(dateString: Date): string {
  const date = DateTime.fromJSDate(dateString).toLocaleParts({
    day: 'numeric',
    month: 'long',
    weekday: 'long',
    year: 'numeric',
  })
  const fixedDate: string[] = []
  date.forEach((element) => {
    if (element.type === 'day') {
      const int = +element.value
      fixedDate.push(`${int}${getDateSuffix(int)}`)
    } else {
      fixedDate.push(element.value) // Rest of values into array in order
    }
  })
  return fixedDate.join('')
}

/**
 *  Function to validate frontmatter values against the schema.
 *
 * @param   {object}  properties    Frontmatter that we need to validate.
 * @param   {object}  schema        Schema to validate against.
 * @param   {string}  whichSchema   String for error message.
 */
export function validateAgainstSchema(
  properties: Record<string, unknown>,
  schema: Record<string, Record<string, unknown>>,
  whichSchema: string
): void {
  const ajv = new Ajv()
  const validator = ajv.compile(schema)
  if (!validator(properties)) {
    for (const err of validator.errors as DefinedError[]) {
      console.error(err)
    }
    throw new ValidationError(`Validation against ${whichSchema} schema returned errors.`)
  }
}

/**
 * Returns an object containing parsed YAML frontmatter (and Markdown content).
 * Fails if frontmatter does not pass validation using provided schema.
 *
 * @param   {string}  markdown      The Markdown content to be parsed.
 * @param   {object}  customSchema  A custom JSONSchema to use
 * @returns {object}                An object containing the parsed metadata.
 */
export default function getFrontmatter(
  markdown: string,
  customSchema?: Record<string, Record<string, unknown>>
): GrayMatterFile<string> {
  const schema: Record<string, Record<string, unknown>> = customSchema ? customSchema : builtInSchema
  const whichSchema = customSchema ? 'custom' : 'built in'

  const splitContent = matter(markdown)

  validateAgainstSchema(splitContent.data, schema, whichSchema)

  // Importing the date from frontmatter will have converted it into a JS Date object. We should send a proper date string.
  if (Object.prototype.hasOwnProperty.call(splitContent.data, 'date')) {
    splitContent.data.date = fixDate(splitContent.data.date as Date)
  }

  return splitContent
}
