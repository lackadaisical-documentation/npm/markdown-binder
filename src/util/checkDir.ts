'use strict'
/**
 * BEGIN HEADER
 *
 * Contains:        Utility class
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This file contains a function to validate the existance of a directory.
 *
 * END HEADER
 */

import fs from 'fs'

/**
 * Validates that a provided directory exists and throws an error if it does not.
 *
 * @param   {string}  directory   Directory to be returned
 */
export default function checkDir(directory: string): void {
  fs.access(directory, (error) => {
    if (error) {
      throw `Path ${directory} is not valid.`
    } else {
      console.log(`Path ${directory} validated.`)
    }
  })
}
