'use strict'
/**
 * BEGIN HEADER
 *
 * Contains:        Utility class
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This file contains a function that returns the appropriate english suffix for a number.
 *
 * END HEADER
 */

/**
 * Returns the appropriate english suffix for a number (st, rd, nd, th)
 *
 * @param   {number}  num       The day that we need a suffix for.
 * @returns  {string}            A string containing the day suffix.
 */
export default function getDateSuffix(num: number): string {
  const th = 'th'
  const rd = 'rd'
  const nd = 'nd'
  const st = 'st'

  if (num === 11 || num === 12 || num === 13) return th

  const lastDigit = num.toString().slice(-1)

  switch (lastDigit) {
    case '1':
      return st
    case '2':
      return nd
    case '3':
      return rd
    default:
      return th
  }
}
