'use strict'
/**
 * BEGIN HEADER
 *
 * Contains:        bindMarkdown command
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This command processes all Markdown files in a directory, performs operations on the content and concatenates them.
 *
 *                  - YAML frontmatter is validated against the built-in schema, or a passed JSONSchema object.
 *                  - Optionally, a document revision history table can be generated from Semver git tags;
 *                    - The folder containing the documents does not have to be the root of a git repository.
 *                    - You must provide an an object containing approvers.
 *                  - A pandoc defaults file will be generated from frontmatter values and an optional passed object containing metadata.
 *                    - This object is parsed (and validated) in the same way as a pandoc defaults file, so you can just use all root
 *                        keys and let the makeDefaultsFile worry about the structure.
 *
 * END HEADER
 */

import getRevisionHistory, { Approvers } from '@lackadaisical/revision-history'
import makeDefaultsFile, { Defaults, defaultsFile } from '@lackadaisical/defaults-generator'
import processMarkdown, { documentContent } from '@lackadaisical/markdown-processor'
import { writeFileAsyncEncoding } from '@lackadaisical/file-utils'
import { GrayMatterFile } from 'gray-matter'
import os from 'os'
import path from 'path'
import sanitize from 'sanitize-filename'
import YAML from 'yaml'

import getFrontmatter from './content/getFrontmatter'

interface BoundMarkdown {
  documentContent: documentContent
  documentFrontmatter: GrayMatterFile<string>
  pandocConfiguration: defaultsFile
}

interface BinderInput {
  customMetadata?: Record<string, unknown>
  defaultsBase?: Record<string, unknown>
  revisionHistory?: Approvers
  customSchema?: Record<string, Record<string, unknown>>
}

// eslint-disable-next-line jsdoc/require-param
/**
 * Function to process a file, or array of files, into MarkDown output.
 * Performs logic on each file's content to ensure that footnotes are unique
 * and that image paths are absolute.
 *
 * @param   {string[]|string}   input                          File(s) to process.
 * @param   {object}            param0                         Destructured paramater object.
 * @param   {object}            param0.customMetadata          Custom metadata object (project settings / defaults).
 * @param   {object}            param0.defaultsBase            Base pandoc defaults configuration (overridden by custom metadata and document frontmatter)
 * @param   {object}            param0.revisionHistory         Optional boolean paramater intructing the binder to produce a DocumentRevisionHistory page.
 * @param   {object}            param0.customSchema            JSONSchema to validate frontmatter against.
 * @returns {object}     Object containing:
 *                         - Processed Markdown file
 *                           Content
 *                           Encoding
 *                         - Frontmatter
 *                         - Pandoc defaults file
 */
export default async function bindMarkdown(
  input: string[] | string,
  {
    customMetadata = undefined,
    defaultsBase = undefined,
    revisionHistory = undefined,
    customSchema = undefined,
  }: BinderInput = {}
): Promise<BoundMarkdown> {
  let contents: documentContent = {
    content: '',
    encoding: '',
  }
  let rootDir = ''

  if (Array.isArray(input)) {
    contents = await processMarkdown(input)
    rootDir = path.resolve(input[0])
  } else if (typeof input === 'string') {
    contents = await processMarkdown([input])
    rootDir = path.resolve(input)
  }

  // Todo: Parse *all* YAML blocks within concatenated input.`
  const frontmatter = getFrontmatter(contents.content, customSchema)
  const defaultsOptions: Defaults = {
    additionalConfig: customMetadata,
    projectSettings: defaultsBase,
  }

  const pandocDefaults = makeDefaultsFile(frontmatter.data, defaultsOptions)

  let revisionPage = ''
  if (revisionHistory) {
    revisionPage = await getRevisionHistory(rootDir, revisionHistory)
  }

  contents.content = revisionHistory ? [revisionPage, frontmatter.content].join(os.EOL) : frontmatter.content

  const finalContent: BoundMarkdown = {
    documentContent: contents,
    documentFrontmatter: frontmatter,
    pandocConfiguration: pandocDefaults,
  }
  return finalContent
}

type OutputObj = {
  [x: string]: {
    name: string
    content: string
    description: string
    path: string
    encoding: string
  }
}

/**
 * Function to write MarkDown output, along with optional pandoc defaults file
 * and YAML metadata file. YAML metadata block may optionally be stripped from the output.
 *
 * @param   {object}    input           Content returned from BindMarkdown
 * @param   {string}    outPath           Path to output directory
 * @param   {boolean}   writeClean        Optional boolean paramater intructing writer to strip YAML frontmatter from Markdown output.
 * @param   {boolean}   writeDefault      Optional boolean paramater intructing writer to write pandoc defaults file
 * @param   {boolean}   writeYaml         Optional boolean paramater intructing writer to write separate YAML metadata file
 */
export async function writeOut(
  input: BoundMarkdown,
  outPath: string,
  writeClean?: boolean | undefined,
  writeDefault?: boolean | undefined,
  writeYaml?: boolean | undefined
): Promise<void> {
  const output: OutputObj = {}

  const titleString =
    'title' in input.documentFrontmatter.data
      ? sanitize((input.documentFrontmatter.data.title as string).replace(/\s+/g, '_'), {
          replacement: '-',
        })
      : sanitize(outPath.split(path.sep).slice(-1).toString().replace(/\s+/g, '_'), { replacement: '-' })

  if (writeClean) {
    output.document = {
      content: input.documentContent.content,
      description: 'Markdown output',
      name: titleString + '.md',
      path: outPath,
      encoding: input.documentContent.encoding,
    }
  } else {
    output.document = {
      content:
        `---${os.EOL}` +
        `${YAML.stringify(input.documentFrontmatter.data)}` +
        `---${os.EOL}${os.EOL}` +
        `${input.documentContent.content}`,
      description: 'Markdown output with frontmatter',
      name: titleString + '.md',
      path: outPath,
      encoding: input.documentContent.encoding,
    }
  }

  if (writeDefault) {
    output.default = {
      content: YAML.stringify(input.pandocConfiguration),
      description: 'pandoc defaults file',
      name: titleString + '.pd',
      path: outPath,
      encoding: 'utf8',
    }
  }

  if (writeYaml) {
    output.yaml = {
      content: YAML.stringify(input.documentFrontmatter),
      description: 'pandoc metadata-file (YAML)',
      name: titleString + '.yaml',
      path: outPath,
      encoding: 'utf8',
    }
  }

  for (const property in output) {
    const propertyValues = output[property]
    await writeFileAsyncEncoding(
      path.join(propertyValues.path, propertyValues.name),
      propertyValues.content,
      propertyValues.encoding
    )
    console.log(`Wrote ${propertyValues.description} to ${path.join(propertyValues.path, propertyValues.name)}`)
  }
}
