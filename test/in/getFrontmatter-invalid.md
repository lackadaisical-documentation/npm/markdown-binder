---
title: "Pandoc and LaTeX Guide"
author:
  - The Lackadaisical Team
date: 2020-06-23
keywords:
  - Lackadaisical
  - Tutorial
subject: Lackadaisical Tutorial

title-page: true
title-page-background: /working/templates/template_resources/lackadaisical_title.pdf
block-subheadings: true
table-row-highlighting: true

---

# Installing Pandoc and LaTeX

Lackadaisical works best with Pandoc and LaTeX. Both are free to use and work on all operating systems. We recommend you install them as soon as possible in order to be able to export all your files and work better with your colleagues. Even after switching to Markdown, you will have to deal with Word-documents and need to share your thoughts with people who do not use Markdown.
