---
title: Pandoc and LaTeX Guide
author:
  - The Lackadaisical Team
keywords:
  - Lackadaisical
  - Tutorial
subject: Lackadaisical Tutorial
title-page: true
title-page-background: /working/templates/template_resources/lackadaisical_title.pdf
block-subheadings: true
table-row-highlighting: true
---


# Heading

Text

## Another Heading

More Text
