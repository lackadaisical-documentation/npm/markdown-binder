/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import fs from 'fs'
import path from 'path'
import test from 'ava'
import execa from 'execa'
import { tmpdir } from 'os'
import { Extract } from 'unzipper'
import YAML from 'yaml'

const cliExec = path.resolve('dist/src/cli.js')
const outPath = path.resolve(tmpdir(), 'cli')

process.chdir('./test')

test.before(async (t) => {
  fs.mkdirSync(outPath, { recursive: true })
  await new Promise<void>((resolve, reject) => {
    fs.createReadStream(path.resolve('./in/annotated.zip'))
      .pipe(Extract({ path: outPath }))
      .on('close', () => resolve())
      .on('error', (error) => reject(error))
  })
})

test('--version option', async (t) => {
  const result = await execa(cliExec, ['--version'], {
    stripFinalNewline: false,
  })
  t.regex(result.stdout, /^\d+\.\d+\.\d+\n$/)
  t.is(result.stderr, '')
})

test('Generates expected output when binding files.', async (t) => {
  const outDir = path.resolve(outPath, 'test-basic')
  fs.mkdirSync(outDir, { recursive: true })
  await execa(cliExec, ['-i', './in/multiple-markdown-files-with-frontmatter/', '-o', outDir])
  t.is(
    fs.readFileSync(path.resolve(path.join(outDir, 'Pandoc_and_LaTeX_Guide.md')), 'utf-8'),
    fs.readFileSync(path.resolve('./out/multiple-markdown-files-with-frontmatter/out.md'), 'utf-8')
  )
})

test('Throws an error when input dir and output dir are the same.', async (t) => {
  const filePath = path.join(outPath, 'annotated')
  const result = await execa(cliExec, ['-i', filePath, '-o', filePath])
  t.is(result.stderr, 'Error: Input directory must not be output directory.')
})

test('Generates a defaults file from frontmatter when asked', async (t) => {
  const outDir = path.resolve(outPath, 'defaults-file')
  const inDir = path.resolve('./in/multiple-markdown-files-with-frontmatter/')
  fs.mkdirSync(outDir, { recursive: true })
  await execa(cliExec, ['-i', inDir, '-o', outDir, '-w'])
  const pandocFile = {
    metadata: {},
    variables: {
      author: ['The Lackadaisical Team'],
      'block-subheadings': true,
      keywords: ['Lackadaisical', 'Tutorial'],
      title: 'Pandoc and LaTeX Guide',
      subject: 'Lackadaisical Tutorial',
      'title-page': true,
      'title-page-background': '/working/templates/template_resources/lackadaisical_title.pdf',
      'table-row-highlighting': true,
    },
  }

  t.deepEqual(YAML.parse(fs.readFileSync(path.join(outDir, 'Pandoc_and_LaTeX_Guide.pd'), 'utf-8')), pandocFile)
})

test('Generates a defaults file from custom metadata when asked', async (t) => {
  const outDir = path.resolve(outPath, 'custom-metadata')
  const inDir = path.resolve('./in/multiple-markdown-files-with-frontmatter/')
  const customMetadata = path.resolve('./in/valid-frontmatter.yaml')
  fs.mkdirSync(outDir, { recursive: true })
  await execa(cliExec, ['-i', inDir, '-o', outDir, '-m', customMetadata, '-w'])
  const pandocFile = {
    metadata: {},
    variables: {
      author: ['The Lackadaisical Team'],
      aspectratio: 169,
      'background-image': true,
      'block-subheadings': true,
      keywords: ['Lackadaisical', 'Tutorial'],
      title: 'Pandoc and LaTeX Guide',
      subject: 'Lackadaisical Tutorial',
      'title-page': true,
      'title-page-background': '/working/templates/template_resources/lackadaisical_title.pdf',
      'table-row-highlighting': true,
    },
    'slide-level': 2,
    toc: true,
  }

  t.deepEqual(YAML.parse(fs.readFileSync(path.join(outDir, 'Pandoc_and_LaTeX_Guide.pd'), 'utf-8')), pandocFile)
})

test('Generates a defaults file from defaults-base when asked', async (t) => {
  const outDir = path.resolve(outPath, 'defaults-base')
  const inDir = path.resolve('./in/multiple-markdown-files-with-frontmatter/')
  const customMetadata = path.resolve('./in/valid-frontmatter.yaml')
  fs.mkdirSync(outDir, { recursive: true })
  await execa(cliExec, ['-i', inDir, '-o', outDir, '-d', customMetadata, '-w'])
  const pandocFile = {
    metadata: {},
    variables: {
      author: ['The Lackadaisical Team'],
      aspectratio: 169,
      'background-image': true,
      'block-subheadings': true,
      keywords: ['Lackadaisical', 'Tutorial'],
      title: 'Pandoc and LaTeX Guide',
      subject: 'Lackadaisical Tutorial',
      'title-page': true,
      'title-page-background': '/working/templates/template_resources/lackadaisical_title.pdf',
      'table-row-highlighting': true,
    },
    'slide-level': 2,
    toc: true,
  }

  t.deepEqual(YAML.parse(fs.readFileSync(path.join(outDir, 'Pandoc_and_LaTeX_Guide.pd'), 'utf-8')), pandocFile)
})

test.after.always((t) => {
  fs.rm(path.resolve(tmpdir(), 'cli'), { recursive: true, force: true }, (err) => {
    if (err) {
      throw err
    }
  })
})
