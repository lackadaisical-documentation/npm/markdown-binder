'use strict'

import { Approvers } from '@lackadaisical/revision-history'
import { Extract } from 'unzipper'
import { JSONSchemaType } from 'ajv'
import bindMarkdown from '../src/index'
import fs from 'fs'
import getFrontmatter from '../src/content/getFrontmatter'
import test from 'ava'
import path from 'path'
import { tmpdir } from 'os'

process.chdir('./test')

test.before(async (t) => {
  const outPath = path.resolve(tmpdir(), './test-data/extracted/')
  fs.mkdirSync(outPath, { recursive: true })
  await new Promise<void>((resolve, reject) => {
    fs.createReadStream(path.resolve('./in/annotated.zip'))
      .pipe(Extract({ path: outPath }))
      .on('close', () => resolve())
      .on('error', (error) => reject(error))
  })
})

test('getFrontmatter returns frontmatter object from valid input', (t) => {
  const data = fs.readFileSync('./in/getFrontmatter-valid.md', 'utf-8')
  const output = getFrontmatter(data)
  t.true(
    Object.prototype.hasOwnProperty.call(output, 'data') && Object.prototype.hasOwnProperty.call(output, 'content')
  )
})

test('getFrontmatter throws an error if frontmatter fails validation', (t) => {
  const fileContents = fs.readFileSync('./in/getFrontmatter-invalid.md', 'utf-8')
  const error = t.throws(() => {
    getFrontmatter(fileContents), t.is(error.message, 'Frontmatter validation returned errors against Default Schema.')
  })
})

test('getFrontmatter returns frontmatter object from valid input with custom schema', (t) => {
  const data = fs.readFileSync('./in/getFrontmatter-invalid.md', 'utf-8')
  type customSchema = {
    keywords: string[]
    subject: string
    tocdepth: number
  }
  const schema: JSONSchemaType<customSchema> = {
    properties: {
      keywords: {
        items: {
          type: 'string',
        },
        type: 'array',
      },
      subject: {
        type: 'string',
      },
      tocdepth: {
        maximum: 5,
        minimum: 2,
        nullable: true,
        type: 'number',
      },
    },
    required: ['keywords', 'subject'],
    type: 'object',
  }
  t.notThrows(() => {
    getFrontmatter(data, schema), 'Frontmatter validation returned errors.'
  })
})

test('getFrontmatter throws an error if custom schema fails validation', (t) => {
  const fileContents = fs.readFileSync('./in/getFrontmatter-valid.md', 'utf-8')
  const schema = {
    properties: {
      keywords: {
        required: true,
        type: 'number',
      },
      subject: {
        required: true,
        type: 'string',
      },
      title: {
        required: true,
        type: 'string',
      },
      tocdepth: {
        maximum: 5,
        minimum: 2,
        type: 'number',
      },
    },
  }
  const error = t.throws(() => {
    getFrontmatter(fileContents, schema), t.is(error.message, 'Validation against custom schema returned errors.')
  })
})

test('bindMarkdown concatenates an array of files', async (t) => {
  await t.notThrowsAsync(
    bindMarkdown([
      './in/multiple-markdown-files-with-frontmatter/001.md',
      './in/multiple-markdown-files-with-frontmatter/002.md',
    ])
  )
})

test('bindMarkdown processes a single file', async (t) => {
  await t.notThrowsAsync(bindMarkdown('./in/multiple-markdown-files-with-frontmatter/001.md'))
})

test('Revision history is returned when requested', async (t) => {
  const approvers: Approvers = {
    'Matt Jolly': {
      position: 'Creator',
      fromDate: '1990-01-01',
      toDate: '2077-01-01',
    },
  }
  const inputPath = path.resolve(tmpdir(), './test-data/extracted/annotated')
  const output = await bindMarkdown([path.join(inputPath, 'Science.md')], { revisionHistory: approvers })
  t.regex(output.documentContent.content, /Document Revision History/)
})

test('Revision history is not returned when not requested', async (t) => {
  const output = await bindMarkdown([
    './in/multiple-markdown-files-with-frontmatter/001.md',
    './in/multiple-markdown-files-with-frontmatter/002.md',
  ])
  t.notRegex(output.documentContent.content, /Document Revision History/)
})

test('Handles more complex input for defaults generation', async (t) => {
  const output = await bindMarkdown([
    './in/multiple-markdown-files-with-frontmatter/001.md',
    './in/multiple-markdown-files-with-frontmatter/002.md',
  ])
  const pandocFile = {
    metadata: {},
    variables: {
      author: ['The Lackadaisical Team'],
      'block-subheadings': true,
      keywords: ['Lackadaisical', 'Tutorial'],
      title: 'Pandoc and LaTeX Guide',
      subject: 'Lackadaisical Tutorial',
      'title-page': true,
      'title-page-background': '/working/templates/template_resources/lackadaisical_title.pdf',
      'table-row-highlighting': true,
    },
  }

  t.deepEqual(output.pandocConfiguration, pandocFile)
})

test.after.always((t) => {
  fs.rm(path.resolve(tmpdir(), './test-data'), { recursive: true, force: true }, (err) => {
    if (err) {
      throw err
    }
  })
})
