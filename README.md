# @lackadaisical/markdown-binder

A module that concatenates Markdown output for use with pandoc.

## Installation and Usage

npm:

`npm install @lackadaisical/markdown-binder`

Yarn:

`yarn add @lackadaisical/markdown-binder`

### As a module

```TypeScript
export default async function bindMarkdown(
  input: string | string[],
  customMetadata?: Record<string, unknown>,
  defaultsBase?: Record<string, unknown>,
  revisionHistory?: Record<string, Record<string, unknown>>,
  customSchema?: Record<string, Record<string, unknown>>
): Promise<boundMarkdown> {

```

Where:

`input` is:

- an array of pathlike strings or;
- a pathlike string.

Optional Params:

  ```TypeScript
  customMetadata?: Record<string, unknown>,
  defaultsBase?: Record<string, unknown>,
  revisionHistory?: Record<string, Record<string, unknown>>,
  customSchema?: Record<string, Record<string, unknown>>
  ```

customMetadata: The content of a pandoc custom metadata-file, or list of keys to be treated as metadata values.

defaultsBase: The content of a pandoc defaults file to be used as the base for configuration.

revisionHistory: An object containing Approvers, their position, and the dates that they are approving documents.

customSchema: An object containing a JSONSchema to validate frontmatter values against.

<!-- ```JSON
{
  "properties": {
    "customMetadata": {
      "type": "object",
      "description": "Custom metadata object to be used when generating pandoc defaults"
    },
    "customSchema": {
      "type": "object"
    },
    "revisionHistory": {
      "type": "object"
    },
}
``` -->

Returns:

An object containing:

- Processed Markdown file
- Frontmatter
- Pandoc defaults file

```JSON
{
  "properties": {
    "documentContent": {
      "content": {
        "description": "Concatenated Markdown content.",
        "type": "string",
      },
      "encoding": {
        "description:": "Encoding for source file(s).",
        "type": "String"
      }
    },
    "documentFrontmatter": {
      "description": "Object containing frontmatter keys.",
      "type": "object"
    },
    "pandocConfiguration": {
      "description": "The content of a pandoc defaults file, validated against a built-in schema",
      "type": "object"
    }
  }
}
```

### CLI

This script will process all Markdown folders within a specified directory (and all subdirectories).

It will concatenate the files after doing a natural sort on the file paths.

Any images in each file will have their paths made absolute in the output. Footnotes are also made unique on a per-file basis to ensure that no collisions occur.

YAML frontmatter is required, and the provided values are validated against an inbuilt schema.

The output file name will be based on the 'title' value. If multiple YAML blocks exist, values will be extracted & validated from the first.

The following outlines the frontmatter schema:

|  **Value**  | **Type** | **Required** | **Requirements** |
| ----------- | :------: | :----------: | :--------------: |
| `keywords`  |  array   |    `true`    |                  |
| `subject`   |  string  |    `true`    |                  |
| `title`     |  string  |    `true`    |                  |
| `toc-depth` |  number  |   `false`    |      1 - 5       |

### Usage

Run the cli:

```bash
markdown-binder -i «path/to/input-dir»
```

By default, the script will output to `input-directory/build`, however you may use the -o option to set an alternate output path.

The script will fail (by design) if the output folder does not exist.

Other flags:

|              Flag              |                         Description                         |              Type              |
| ------------------------------ | ----------------------------------------------------------- | ------------------------------ |
| `--version`                    | Show version number                                         | `boolean`                      |
| `-i`, `--input-directory`      | Input Directory                                             | `string` **required**          |
| `-c`, `--write-clean`          | Write content without frontmatter                           | `boolean` **default**: `false` |
| `-d`, `--pandoc-defaults-base` | Pandoc defaults base configuration                          | `boolean` **default**: `false` |
| `-f`, `--separate-frontmatter` | Output frontmatter as a separate YAML metadata-file         | `boolean` **default**: `false` |
| `-m`, `--custom-metadata`      | YAML configuration file - overrides pandoc defaults base    | `string`                       |
| `-o`, `--output-directory`     | Output Directory                                            | `string`                       |
| `-r`, `--revision-history`     | Path to YAML file containing approvers for revision history | `string`                       |
| `-s`, `--custom-schema`        | Path to JSONSchema to validate frontmatter against          | `string`                       |
| `-w`, `--write-defaults`       | Write pandoc defaults file (configuration file)             | `boolean`                      |
| `-h`, `--help`                 | Show help                                                   | `boolean`                      |
| `--verbose`                    | Enable verbose output                                       | `boolean` **default**: `false` |

